============
DRAG Builder
============

Ferramenta para actualizar o módulo ``rag/gl/correcto.dic`` automaticamente a
partir do dicionario da Real Academia Galega.

Requisitos
==========

A ferramenta require ter Docker_ instalado.


Uso
===

Cumprido o requisito, débense executar o seguinte desde un terminal aberto no
cartafol no que se atopa este documento::

    docker build -t drag-builder .
    docker run \
        -v "$PWD"/../../src/rag/gl:/app/data \
        -v "$PWD"/cache:/app/cache \
        --rm \
        drag-builder


Caché
=====

A ferramenta crea un cartafol, ``cache``, onde se gardarán:

-   Un cartafol, ``scrapy``, onde se almacena unha caché das páxinas do DRAG
    visitadas.

    Isto permite:

    -   Deter a descarga das páxinas e continuala sen necesidade de descargar
        de novo as páxinas que xa se descargaran, acelerando así o seu
        procesamento.

    -   Repetir o procesamento de todas as páxinas, por exemplo tras cambiar a
        lóxica de procesamento, sen necesidade de descargar de novo todas as
        páxinas, acelerando así o seu procesamento.

-   Un ficheiro, ``drag.jl``, co resultado de procesar as páxinas do DRAG
    extraendo os datos necesarios para a xeración do ficheiro de Hunspell final
    en formato JSON Lines.

    Isto permite repetir a xeración do ficheiro de Hunspell final sen
    necesidade de procesar de novo as páxinas do DRAG.

Existen circunstancias que requiren eliminar todo ou parte do contido do
cartafol ``cache``:

-   Para descargar de novo os últimos datos do DRAG hai que eliminar todo o
    contido de ``cache``.

-   Tras cambios na forma de xerar o ficheiro ``drag.jl``, este ficheiro debe
    primeiro eliminarse para poder xeralo de novo a partir das páxinas do DRAG.

.. note:: Como o cartafol ``cache`` e o seu contido o crea Docker, cartafoles e
          ficheiros estarán asignados ao administrador do sistema (``root``), e
          requiriranse permisos de administrador para eliminalos.


.. _Docker:: https://docs.docker.com/install/
