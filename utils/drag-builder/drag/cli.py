from pathlib import Path

from drag.hunspell import jl_to_dic
from drag.scraping import scrape


def update():
    app_dir = Path('/app')
    cache_dir = app_dir / 'cache'
    jl_filepath = cache_dir / 'drag.jl'
    dic_filepath = app_dir / 'data/correcto.dic~'

    if not jl_filepath.exists():
        scrape(jl_filepath, cache_dir)

    jl_to_dic(jl_filepath, dic_filepath)
