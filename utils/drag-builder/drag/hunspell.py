#!/usr/bin/env python3

import json
import re
from collections import defaultdict
from copy import deepcopy
from enum import Enum
from itertools import chain
from pathlib import Path
from tempfile import mkstemp, TemporaryDirectory

import sh
import yaml
from loguru import logger

from drag.grammar import (
    INFLEXIBLE_POS,
    NO_GENDER_OR_NUMBER_POS,
    NO_NUMBER_POS,
    VERB_POS,
)


def iter_json_lines(filepath):
    with open(filepath) as input_file:
        for line in input_file:
            yield json.loads(line)


ADJ_10_RE = re.compile(
    r'''(?ux)
    (
        [áéíóú][^\W\daáeéiíïoóuúü]+il|  # p. ex. fráxil
        ^[^\W\daáeéiíïoóuúü]*[aeiou]l|  # p. ex. vil
        cortés|
        a|á|ar|ás|e|í|ín|ior|[^o]r|lor|y|z
    )$
    '''
)
ADJ_11_SET = {
    'gángster',
    'hippy',
    'júnior',
    'póster',
    'rally',
    'whisky',
}
ADJ_10_17_RE = re.compile(
    r'''(?ux)
    (
        an|
        deu|
        mao|
        [nr]u|
        [sv]ó
    )$
    '''
)


class Status(Enum):
    RECOMMENDED = 1
    ALLOWED = 2


class Entry:

    def __init__(
        self,
        lemma,
        *,
        feminine_lemmas,
        pos_feminine_lemmas,
        invariable,
        parts_of_speech,
        nested_parts_of_speech,
        same_as,
        better_forms,
    ):
        self.lemma = lemma
        self.feminine_lemmas = set(feminine_lemmas)
        self.pos_feminine_lemmas = {k: set(v) for k, v
                                    in pos_feminine_lemmas.items()}
        self.invariable = invariable
        self.parts_of_speech = set(parts_of_speech)
        self.nested_parts_of_speech = {k: set(v) for k, v in
                                       nested_parts_of_speech.items()}
        self.same_as = same_as
        self.better_forms = better_forms
        if better_forms:
            self.status = Status.ALLOWED
        else:
            self.status = Status.RECOMMENDED

    def json(self):
        return json.dumps(
            {
                "lemma": self.lemma,
                "feminine_lemmas": sorted(self.feminine_lemmas),
                "pos_feminine_lemmas": {k: sorted(v) for k, v in
                                        self.pos_feminine_lemmas.items()},
                "invariable": self.invariable,
                "parts_of_speech": sorted(self.parts_of_speech),
                "nested_parts_of_speech": {k: sorted(v) for k, v in
                                        self.nested_parts_of_speech.items()},
                "same_as": self.same_as,
                "better_forms": self.better_forms,
            }
        )


class Builder:

    def __init__(self, output_path):
        self._build_dir = TemporaryDirectory()
        self._output_path = output_path
        self._po_set_files = {}

    def _add(self, word, flags=tuple(), po=None):
        if isinstance(flags, int):
            flags = (flags,)
        if word != word.lower():
            flags = (*flags, 999)
        if flags:
            flag_string = '/' + ','.join(str(flag) for flag in sorted(flags))
        else:
            flag_string = ''
        if po is None:
            po_set = set()
        else:
            if isinstance(po, str):
                po = (po,)
            po_set = set(po)
        self._pos_being_added |= po_set
        po_set = tuple(sorted(po_set))
        po_string = ''.join(f' po:{_po.replace(" ", "_")}' for _po in po_set)
        line = f'{word}{flag_string}{po_string}\n'
        if po_set not in self._po_set_files:
            base_name = '-'.join(po_set)
            po_set_filepath = Path(self._build_dir.name) / f'{base_name}.dic'
            po_set_file = po_set_filepath.open('w')
            self._po_set_files[po_set] = (po_set_filepath, po_set_file)
        self._po_set_files[po_set][1].write(line)

    def add(self, entry):
        if entry.status != Status.RECOMMENDED:
            return

        self._pos_being_added = set()

        po = 'abreviatura'
        if po in entry.parts_of_speech:
            other_pos = set(entry.parts_of_speech)
            other_pos.remove(po)
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=po)

        pos = {'abreviatura', 'adverbio'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=pos)

        pos = {'abreviatura', 'preposición'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=pos)

        pos = {'abreviatura', 'símbolo'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=pos)

        po = 'adverbio'
        if po in entry.parts_of_speech:
            other_pos = set(entry.parts_of_speech)
            other_pos.remove(po)
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=po)

        pos = {'adverbio', 'conxunción'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=pos)

        pos = {'adverbio', 'conxunción', 'preposición'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=pos)

        pos = {'adverbio', 'indefinido', 'substantivo masculino'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            elif entry.invariable:
                self._add(entry.lemma, po=pos)

        pos = {'adverbio', 'interxección'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=pos)

        pos = {'adverbio', 'interxección', 'preposición'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=pos)

        pos = {'adverbio', 'preposición'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=pos)

        pos = {'adverbio', 'preposición', 'substantivo masculino', 'substantivo plural'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            elif entry.invariable:
                self._add(entry.lemma, po=pos)

        pos = {'adverbio', 'substantivo masculino'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            elif entry.invariable:
                self._add(entry.lemma, po=pos)

        po = 'adverbio latino'
        if po in entry.parts_of_speech:
            other_pos = set(entry.parts_of_speech)
            other_pos.remove(po)
            if other_pos and entry.invariable is True:
                pass
            elif other_pos & INFLEXIBLE_POS:
                pass
            else:
                self._add(entry.lemma, po=po)

        po = 'adxectivo'
        if po in entry.parts_of_speech:
            def handle_adjective(
                *,
                other_pos,
                feminine_lemmas,
                unique_invariability=None,
            ):
                if ADJ_10_RE.search(entry.lemma):
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, po)
                elif entry.lemma in ADJ_11_SET:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 11, po)
                elif ADJ_10_17_RE.search(entry.lemma):
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 17), po)
                elif entry.lemma.endswith('f'):  # p. ex. naíf
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, po=po)
                elif entry.lemma.endswith('án'):
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        if (
                            len(feminine_lemmas) == 1
                            and next(iter(feminine_lemmas)).endswith('ana')
                        ):
                            self._add(entry.lemma, (10, 16), po)
                        else:
                            self._add(entry.lemma, (10, 15), po)
                elif entry.lemma.endswith('és'):
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 15), po)
                elif entry.lemma.endswith('ón'):
                    if not feminine_lemmas:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, po)  # e.g. marrón
                    elif other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 16), po)
                elif entry.lemma.endswith('or'):
                    if feminine_lemmas:
                        if other_pos <= NO_GENDER_OR_NUMBER_POS:
                            feminine_rules = set()
                            for feminine_lemma in feminine_lemmas:
                                if feminine_lemma.endswith('triz'):
                                    feminine_rules.add(15)
                                else:
                                    feminine_rules.add(14)
                            self._add(entry.lemma, (10, *feminine_rules), po)
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, po)
                elif entry.lemma.endswith('ún'):
                    if feminine_lemmas:
                        if other_pos <= NO_GENDER_OR_NUMBER_POS:
                            self._add(entry.lemma, (10, 15), po)
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, po)
                elif entry.lemma.endswith('l'):
                    if feminine_lemmas:
                        if unique_invariability == 'xénero':
                            self._add(entry.lemma, 12, po)
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 12, po)
                elif entry.lemma.endswith('o'):
                    if feminine_lemmas:
                        if other_pos <= NO_GENDER_OR_NUMBER_POS:
                            self._add(entry.lemma, (10, 15), po)
                    else:
                        if other_pos <= NO_NUMBER_POS:
                            self._add(entry.lemma, 10, po)
                elif entry.lemma.endswith('s'):
                    if not other_pos:
                        # p. ex. prínceps
                        self._add(entry.lemma, po=po)
                elif entry.lemma.endswith('t'):
                    if not other_pos:
                        # p. ex. fahrenheit, fondant
                        self._add(entry.lemma, po=po)
                elif entry.lemma.endswith('ú'):
                    if not other_pos:
                        # p. ex. recrú
                        self._add(entry.lemma, (10, 14), po=po)
                elif entry.lemma.endswith('x'):
                    if not other_pos:
                        # p. ex. unisex
                        self._add(entry.lemma, po=po)

            if entry.lemma == 'asistente':
                # Como substantivo permite cambio de xénero: asistenta.
                self._add(entry.lemma, 10, po)
            elif entry.lemma == 'bo':
                self._add(entry.lemma, (10, 14), po)
            elif entry.lemma == 'ciano':
                pass  # Non varía, pero tampouco como substantivo masculino
            elif entry.lemma in {'cisxénero', 'transxénero'}:
                self._add(entry.lemma, po=po)
            elif entry.lemma == 'gran':
                # Non varía.
                self._add(entry.lemma, po=po)
            elif entry.lemma == 'locomotor':
                self._add(entry.lemma, (10, 14, 15), po)
            elif entry.lemma == 'trisector':
                # Como substantivo feminino forma o feminino en -triz.
                self._add(entry.lemma, (10, 14), po)
            else:
                other_pos = set(entry.parts_of_speech)
                other_pos.remove(po)
                if entry.invariable is True:
                    if not other_pos:
                        self._add(entry.lemma, po=po)
                elif (
                    entry.invariable is not False
                    and po in entry.invariable
                ):
                    def get_unique_invariability():
                        for value in (True, 'número', 'xénero'):
                            if entry.invariable[po] == value:
                                if any(
                                    entry.invariable.get(other_po) == value
                                    for other_po in other_pos
                                ):
                                    break
                                return value
                        return None

                    unique_invariability = get_unique_invariability()
                    if unique_invariability is True:
                        self._add(entry.lemma, po=po)
                    elif unique_invariability is not None:
                        handle_adjective(
                            other_pos=other_pos,
                            unique_invariability=unique_invariability,
                            feminine_lemmas=entry.feminine_lemmas,
                        )
                elif entry.pos_feminine_lemmas:
                    feminine_lemmas_to_pos = defaultdict(set)
                    for pos, feminine_lemmas in entry.pos_feminine_lemmas.items():
                        feminine_lemmas_to_pos[tuple(feminine_lemmas)].add(pos)
                    for feminine_lemmas, pos in feminine_lemmas_to_pos.items():
                        if po in pos:
                            pos.remove(po)
                            handle_adjective(
                                other_pos=pos,
                                feminine_lemmas=feminine_lemmas,
                            )
                            break
                        other_pos -= pos
                    else:
                        handle_adjective(
                            other_pos=other_pos,
                            feminine_lemmas=entry.feminine_lemmas,
                        )
                else:
                    handle_adjective(
                        other_pos=other_pos,
                        feminine_lemmas=entry.feminine_lemmas,
                    )

        pos = {'adxectivo', 'artigo indeterminado', 'indefinido', 'numeral'}
        if all(po in entry.parts_of_speech for po in pos):
            if entry.lemma == 'un':
                self._add(entry.lemma, (10, 18), pos)

        pos = {'adxectivo', 'participio'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if entry.invariable is True:
                if not other_pos:
                    self._add(entry.lemma, po=pos)
            elif entry.lemma.endswith('e'):
                if entry.feminine_lemmas:
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 15), pos)
                else:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, pos)
            elif entry.lemma.endswith('o'):
                if entry.feminine_lemmas:
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 15), pos)
                else:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, pos)  # p. ex. preñe

        pos = {'adxectivo', 'participio', 'substantivo'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if entry.invariable is True:
                if not other_pos:
                    self._add(entry.lemma, po=pos)
            elif entry.lemma.endswith('e'):
                if entry.feminine_lemmas:
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 15), pos)
                else:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, pos)
            elif entry.lemma.endswith('o'):
                if entry.feminine_lemmas:
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 15), pos)
                else:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, pos)

        pos = {'adxectivo', 'substantivo'}
        if all(po in entry.parts_of_speech for po in pos):
            other_pos = set(entry.parts_of_speech) - pos
            if entry.invariable is True:
                if not other_pos:
                    self._add(entry.lemma, po=pos)
            elif ADJ_10_RE.search(entry.lemma):
                if not entry.feminine_lemmas:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, pos)
                elif other_pos <= NO_GENDER_OR_NUMBER_POS:
                    self._add(entry.lemma, (10, 14), pos)
            elif entry.lemma.endswith('án'):
                if other_pos <= NO_GENDER_OR_NUMBER_POS:
                    if (
                        len(entry.feminine_lemmas) == 1
                        and next(iter(entry.feminine_lemmas)).endswith('ana')
                    ):
                        self._add(entry.lemma, (10, 16), pos)
                    else:
                        self._add(entry.lemma, (10, 15), pos)
            elif entry.lemma.endswith('ao'):
                if other_pos <= NO_GENDER_OR_NUMBER_POS:
                    if (
                        len(entry.feminine_lemmas) == 1
                        and next(iter(entry.feminine_lemmas)).endswith('á')
                    ):
                        self._add(entry.lemma, (10, 16), pos)
                    else:
                        self._add(entry.lemma, (10, 15), pos)
            elif entry.lemma.endswith('és'):
                if other_pos <= NO_GENDER_OR_NUMBER_POS:
                    self._add(entry.lemma, (10, 15), pos)
            elif entry.lemma.endswith('ón'):
                if not entry.feminine_lemmas:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, pos)
                elif other_pos <= NO_GENDER_OR_NUMBER_POS:
                    if (
                        len(entry.feminine_lemmas) == 1
                        and next(iter(entry.feminine_lemmas)).endswith('oa')
                    ):
                        self._add(entry.lemma, (10, 15), pos)
                    else:
                        self._add(entry.lemma, (10, 16), pos)
            elif entry.lemma.endswith('or'):
                if entry.feminine_lemmas:
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        feminine_rules = set()
                        for feminine_lemma in entry.feminine_lemmas:
                            if feminine_lemma.endswith('triz'):
                                feminine_rules.add(15)
                            else:
                                feminine_rules.add(14)
                        self._add(entry.lemma, (10, *feminine_rules), pos)
                else:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, pos)
            elif entry.lemma.endswith('e'):
                if entry.feminine_lemmas:
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 15), pos)
                else:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, pos)
            elif entry.lemma.endswith('l'):
                if other_pos <= NO_NUMBER_POS:
                    self._add(entry.lemma, 12, pos)
            elif entry.lemma.endswith('o'):
                if entry.feminine_lemmas:
                    if other_pos <= NO_GENDER_OR_NUMBER_POS:
                        self._add(entry.lemma, (10, 15), pos)
                else:
                    if other_pos <= NO_NUMBER_POS:
                        self._add(entry.lemma, 10, pos)

        for known_pos in (
            'abreviatura',
            'adverbio',
            'adverbio_latino',
            #'adxectivo',
            #'adxectivo feminino',
            #'adxectivo masculino',
            #'artigo',
            #'artigo determinado',
            #'artigo indeterminado',
            #'indefinido',
            #'indefinido plural',
            #'numeral',
            #'participio',
            #'preposición',
            #'pronome',
            #'pronome persoal',
            #'substantivo',
            #'substantivo feminino',
            #'substantivo masculino',
        ):
            if (
                known_pos in entry.parts_of_speech
                and known_pos not in self._pos_being_added
            ):
                print(f'Aviso: a función como {known_pos.replace("_", " ")} '
                      f'de «{entry.lemma}» non casou con ningunha regra. '
                      f'Categorías gramaticais: {entry.parts_of_speech!r}')

    def build(self, input_path):
        for data in iter_json_lines(input_path):
            self.add(Entry(**data))
        with self._output_path.open('w') as output:
            for po_set in sorted(self._po_set_files):
                po_set_filepath, po_set_file = self._po_set_files.pop(po_set)
                po_set_file.close()
                sh.sort(
                    str(po_set_filepath),
                    _env={'LC_ALL': 'gl_ES.UTF-8'},
                    _out=output,
                )


def load_data(filename, key):
    input_path = Path(__file__).parent / f'{filename}.yml'
    with input_path.open('rb') as input_file:
        data = yaml.load(input_file, Loader=yaml.SafeLoader)
    return data[key], f'{input_path}:{key}'


def normalize_parts_of_speech(input_path):

    def split_parts_of_speech(parts_of_speech):
        for part_of_speech in parts_of_speech:
            if ' e ' in part_of_speech:
                part_of_speech, part_of_speech_2 = part_of_speech.rsplit(
                    ' e ',
                    maxsplit=1,
                )
                yield part_of_speech_2
            yield from part_of_speech.split(', ')

    renamings, _ = load_data('config', 'part_of_speech_renamings')
    _, output_path = mkstemp()
    with open(output_path, 'w') as output_file:
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            parts_of_speech = {
                renamings.get(part_of_speech, part_of_speech)
                for part_of_speech in split_parts_of_speech(
                    entry.parts_of_speech
                )
            }
            if 'singular' in parts_of_speech:
                parts_of_speech.remove('singular')
                found_plurals = False
                for part in list(parts_of_speech):
                    if part.endswith(' plural'):
                        parts_of_speech.remove(part)
                        parts_of_speech.add(part[:-7])
                        found_plurals = True
                if not found_plurals:
                    logger.error(
                        f'Na palabra {entry.lemma}: Atopouse «singular» como '
                        f'categoría gramatical pero ningunha das outras '
                        f'categorías gramaticais ({parts_of_speech}) son '
                        f'plurais.'
                    )
            entry.parts_of_speech = parts_of_speech
            output_file.write(f'{entry.json()}\n')
    return output_path


def fix_parts_of_speech(input_path):
    fixes, reference = load_data('fixes', 'parts_of_speech')
    fixes = {
        k: (
            set(v['seen']),
            set(v['expected']),
        )
        for k, v in fixes.items()
    }
    _, output_path = mkstemp()
    with open(output_path, 'w') as output_file:
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if entry.lemma in fixes:
                seen, expected = fixes[entry.lemma]
                if entry.parts_of_speech == seen:
                    entry.parts_of_speech = expected
                elif entry.parts_of_speech == expected:
                    logger.info(
                        f'A RAG corrixiu as categorías gramaticais de '
                        f'{entry.lemma}, pode retirarse o código de '
                        f'{reference} que as corrixe.'
                    )
                else:
                    logger.info(
                        f'A RAG cambiou as categorías gramaticais de '
                        f'{entry.lemma}, pero en vez de ao esperado '
                        f'({expected}) cambiounas a {entry.parts_of_speech}. '
                        f'Cómpre revisar o código de {reference} que as '
                        f'corrixía, por se pode retirarse ou debe modificarse '
                        f'para corrixir as novas categorías gramaticais, e '
                        f'neste último caso cómpre avisar á RAG.'
                    )
            output_file.write(f'{entry.json()}\n')
    return output_path


def disambiguate_parts_of_speech(input_path):
    data, reference = load_data('config', 'part_of_speech_disambiguations')
    part_of_speech_disambiguations = tuple(
        (
            set(entry['input']),
            set(entry['output']),
        )
        for entry in data
    )
    _, output_path = mkstemp()
    with open(output_path, 'w') as output_file:
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            for replaced, replacements in part_of_speech_disambiguations:
                if (
                    (
                        all(
                            part_of_speech in entry.parts_of_speech
                            for part_of_speech in replaced
                        )
                        or (
                            any(
                                part_of_speech in entry.parts_of_speech
                                for part_of_speech in replaced
                            )
                            and all(
                                part_of_speech in entry.parts_of_speech
                                for part_of_speech in replacements
                            )
                        )
                    )
                    and not any(
                        part_of_speech in entry.pos_feminine_lemmas
                        for part_of_speech in replaced
                    )
                ):
                    entry.parts_of_speech -= replaced
                    entry.parts_of_speech |= replacements
            output_file.write(f'{entry.json()}\n')
    return output_path


def fix_feminine_lemmas(input_path):
    fixes, reference = load_data('fixes', 'feminine')
    _, output_path = mkstemp()
    with open(output_path, 'w') as output_file:
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if entry.lemma in fixes:
                expected = fixes[entry.lemma]
                if isinstance(expected, str):
                    expected = [expected]
                expected = set(expected)
                if entry.feminine_lemmas != expected:
                    entry.feminine_lemmas = expected
                else:
                    logger.info(
                        f'A RAG corrixiu os lemas femininos de {entry.lemma}, '
                        f'pode retirarse o código de {reference} que os '
                        f'corrixe.'
                    )
            output_file.write(f'{entry.json()}\n')
    return output_path


def fix_invariable(input_path):
    fixes, reference = load_data('fixes', 'invariable')
    _, output_path = mkstemp()
    with open(output_path, 'w') as output_file:
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if entry.lemma in fixes:
                expected = fixes[entry.lemma]
                if entry.invariable != expected:
                    entry.invariable = expected
                else:
                    logger.info(
                        f'A RAG corrixiu a invariabilidade de {entry.lemma}, '
                        f'pode retirarse o código de {reference} que o '
                        f'corrixe.'
                    )
            entry.invariable = (
                entry.invariable
                or entry.lemma.endswith('as')  # p. ex. alias
                or entry.lemma.endswith('os')  # p. ex. menos
            )
            output_file.write(f'{entry.json()}\n')
    return output_path


def fix_same_as(input_path):
    fixes, reference = load_data('fixes', 'same_as')
    fixes = {
        k: (
            v['seen'],
            v['expected'],
        )
        for k, v in fixes.items()
    }
    _, output_path = mkstemp()
    with open(output_path, 'w') as output_file:
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if entry.lemma in fixes:
                seen, expected = fixes[entry.lemma]
                if entry.same_as == seen:
                    entry.same_as = expected
                elif entry.same_as == expected:
                    logger.info(
                        f'A RAG corrixiu o lemma de referencia de '
                        f'{entry.lemma}, pode retirarse o código de '
                        f'{reference} que o corrixe.'
                    )
                else:
                    logger.info(
                        f'A RAG cambiou o lemma de referencia de '
                        f'{entry.lemma}, pero en vez de ao esperado '
                        f'({expected}) cambiouno a {entry.same_as}. Cómpre '
                        f'revisar o código de {reference} que o corrixía, '
                        f'por se pode retirarse ou debe modificarse '
                        f'para corrixir o novo lema de referencia, e '
                        f'neste último caso cómpre avisar á RAG.'
                    )
            output_file.write(f'{entry.json()}\n')
    return output_path


def apply_same_as(input_path):
    fillers = defaultdict(set)
    incomplete = set()
    _, output_path = mkstemp()
    with open(output_path, 'w') as output_file:
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if entry.same_as:
                fillers[entry.same_as].add(entry)
                incomplete.add(entry.lemma)
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if entry.lemma not in incomplete:
                output_file.write(f'{entry.json()}\n')
            if entry.lemma in fillers:
                incomplete_entries = fillers.pop(entry.lemma)
                for incomplete_entry in incomplete_entries:
                    for part_of_speech in list(incomplete_entry.parts_of_speech):
                        if part_of_speech in entry.nested_parts_of_speech:
                            incomplete_entry.parts_of_speech |= entry.nested_parts_of_speech[part_of_speech]
                    output_file.write(f'{incomplete_entry.json()}\n')
    if fillers:
        incomplete_entry_list = '\n'.join(
            sorted(
                f'{entry.lemma} fai referencia a {reference}'
                for reference, entries in fillers.items()
                for entry in entries
            )
        )
        logger.warning(
            f'Para algunhas entradas do dicionario que facían referencia '
            f'a outras para definicións non se atopou a entrada á que '
            f'facían referencia:\n{incomplete_entry_list}'
        )
    return output_path


def detect_participles(input_path):
    verb_to_participles = defaultdict(set)
    new_participles = set()
    _, output_path = mkstemp()
    with open(output_path, 'w') as output_file:
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            potential_verbs = set()
            if (
                'adxectivo' in entry.parts_of_speech
                and 'participio' not in entry.parts_of_speech
            ):
                if entry.lemma.endswith('ado'):
                    potential_verbs = {
                        entry.lemma[:-3] + 'ar',
                    }
                elif entry.lemma.endswith('ido'):
                    potential_verbs = {
                        entry.lemma[:-3] + 'er',
                        entry.lemma[:-3] + 'ir',
                    }
            for verb in potential_verbs:
                verb_to_participles[verb].add(entry.lemma)
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if (
                entry.lemma in verb_to_participles
                and not entry.parts_of_speech.isdisjoint(VERB_POS)
            ):
                new_participles |= verb_to_participles.pop(entry.lemma)
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if entry.lemma in new_participles:
                entry.parts_of_speech.add('participio')
            output_file.write(f'{entry.json()}\n')
    return output_path


def merge_genders(input_path):

    def get_potential_root(word_form):
        if word_form.endswith('a'):
            return word_form[:-1] + 'o'
        return None

    potential_roots = {}
    feminines = {}
    roots = {}
    _, output_path = mkstemp()
    with open(output_path, 'w') as output_file:
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            potential_root = get_potential_root(entry.lemma)
            if potential_root is None:
                continue
            if (
                all('feminino' in pos for pos in entry.parts_of_speech)
                and entry.status == Status.RECOMMENDED
            ):
                potential_roots[potential_root] = entry.lemma
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if entry.lemma in potential_roots and not entry.invariable:
                feminines[potential_roots[entry.lemma]] = entry.lemma
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if entry.lemma in feminines:
                roots[feminines[entry.lemma]] = entry
        for data in iter_json_lines(input_path):
            entry = Entry(**data)
            if entry.lemma in roots:
                feminine_entry = roots.pop(entry.lemma)
                inflexible_pos = feminine_entry.parts_of_speech & INFLEXIBLE_POS
                if inflexible_pos:
                    inflexible_feminine_entry = deepcopy(feminine_entry)
                    inflexible_feminine_entry.parts_of_speech = inflexible_pos
                    output_file.write(f'{inflexible_feminine_entry.json()}\n')
                flexible_pos = feminine_entry.parts_of_speech - INFLEXIBLE_POS
                if flexible_pos:
                    entry.parts_of_speech |= flexible_pos
            if entry.lemma not in feminines:
                output_file.write(f'{entry.json()}\n')
    return output_path


def jl_to_dic(input_path, output_path):
    input_path = fix_feminine_lemmas(input_path)
    input_path = fix_invariable(input_path)
    input_path = normalize_parts_of_speech(input_path)
    input_path = fix_parts_of_speech(input_path)
    input_path = fix_same_as(input_path)
    input_path = apply_same_as(input_path)
    input_path = detect_participles(input_path)
    input_path = merge_genders(input_path)
    input_path = disambiguate_parts_of_speech(input_path)
    Builder(output_path).build(input_path)
