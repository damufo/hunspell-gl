# Parts of speech that do not allow gender, number or conjugation.
INFLEXIBLE_POS = {
    'abreviatura',
    'adverbio',
    'adverbio latino',
    'adxectivo feminino plural',
    'adxectivo feminino singular',
    'adxectivo masculino plural',
    'adxectivo masculino singular',
    'conxunción',
    'interxección',
    'locución adverbial',
    'locución adverbial latina',
    'locución adxectiva latina',
    'prefixo',
    'preposición',
    'sigla',
    'símbolo',
    'substantivo feminino plural',
    'substantivo feminino singular',
    'substantivo masculino plural',
    'substantivo masculino singular',
}

# Verb parts of speech.
VERB_POS = {
    'verbo intransitivo',
    'verbo pronominal',
    'verbo transitivo',
}

# Parts of speech that allow neither gender nor number.
_NEITHER_GENDER_NOR_NUMBER_POS = INFLEXIBLE_POS | VERB_POS

# Parts of speech that allow no gender.
_NO_GENDER_POS = {
    'adxectivo feminino',
    'adxectivo masculino',
    'locución substantiva feminina',
    'locución substantiva masculina',
    'substantivo feminino',
    'substantivo masculino',
}
_NO_GENDER_POS |= _NEITHER_GENDER_NOR_NUMBER_POS

# Parts of speech that allow no number.
NO_NUMBER_POS = {
    'adxectivo plural',
    'adxectivo singular',
    'indefinido plural',
    'indefinido singular',
    'substantivo plural',
    'substantivo singular',
}
NO_NUMBER_POS |= _NEITHER_GENDER_NOR_NUMBER_POS

# Parts of speech that allow no gender or number.
NO_GENDER_OR_NUMBER_POS = NO_NUMBER_POS | _NO_GENDER_POS
