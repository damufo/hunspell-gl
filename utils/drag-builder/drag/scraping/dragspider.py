import json
import re
from collections import defaultdict
from itertools import chain
from urllib.parse import urlencode

from loguru import logger
from parsel import Selector
import requests
from scrapy import Spider
from scrapy.http import FormRequest

from drag.scraping.basespider import BaseSpider


def api_query(query, data, **kwargs):
    ns = 'dictionarynormalsearch_WAR_BUSCARAGFrontendportlet'
    query = {'p_p_id': ns,
             'p_p_lifecycle': '2',
             **{f'_{ns}_{k}': str(v) for k, v in query.items()}}
    data = {f'_{ns}_{k}': str(v) for k, v in data.items()}
    return FormRequest(f'https://academia.gal/dicionario?{urlencode(query)}',
                       formdata=data, **kwargs)


def get_same_as(selector):
    css_classes = ('Subentry', 'Sense', 'Definition', 'Definition__Definition')
    for css_class in css_classes:
        selector = selector.css(f'.{css_class}')
        if len(selector) > 1:
            return None
    same_as = selector.css(':scope > a::attr(data-noun)').get()
    if not same_as:
        return None
    return same_as.lower()


class DRAGSpider(BaseSpider):
    name = 'drag'

    @classmethod
    def is_online(cls):
        url = 'https://academia.gal/dicionario?p_p_id=dictionarynormalsearch_WAR_BUSCARAGFrontendportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_cacheability=cacheLevelPage&p_p_col_id=column-2&p_p_col_count=1&_dictionarynormalsearch_WAR_BUSCARAGFrontendportlet_cmd=loadNoun&_dictionarynormalsearch_WAR_BUSCARAGFrontendportlet_homonymNumber=&_dictionarynormalsearch_WAR_BUSCARAGFrontendportlet_searchType=normal'
        data = '_dictionarynormalsearch_WAR_BUSCARAGFrontendportlet_noun=galicismo'
        response = requests.post(url, data, verify=False)
        return bool(response.json())

    def entries_request(self, word, direction):
        return api_query(query={'cmd': f'{direction}TermsSearch'},
                         data={'noun': word, 'homonym': '0'},
                         callback=self.parse_entries,
                         meta={'browse': direction})

    def entry_request(self, word, **meta):
        return api_query(query={'cmd': 'loadNoun'},
                         data={'noun': word},
                         callback=self.parse_entry,
                         meta={'word': word, **meta})

    def start_requests(self):
        # Request a word that is somewhere in the middle of the dictionary.
        yield self.entry_request('galicismo', browse='both')

        # Request isolated words, which cannot be reached by following the
        # lists of words that go before or after alphabetically. Dates indicate
        # when issues were reported upstream.
        isolated_words = (
            'oca',  # 2020-08-30
            'solar',  # 2020-08-23
        )
        for word in isolated_words:
            yield self.entry_request(word)

    def parse_entry(self, response):
        browse, word = response.meta.get('browse'), response.meta['word']
        subentries = {}
        feminine_lemmas = set()
        pos_feminine_lemmas = defaultdict(set)
        same_as = None

        for index, subentry in enumerate(json.loads(response.text), start=1):
            if subentry['title'] != word:
                continue

            selector = Selector(subentry['htmlContent'])

            if selector.xpath('//*[@class="References"]'
                              '[re:test(text(), "Formas?\s+correctas?:")]'
                              '//@data-noun'):
                continue

            main_feminine_lemma = subentry['titleGender']
            if main_feminine_lemma:
                feminine_lemmas.add(main_feminine_lemma)

            css = '.Lemma__Feminino_irregular::text'
            alternative_feminine_lemma = selector.css(css).get()
            if alternative_feminine_lemma:
                feminine_lemmas.add(alternative_feminine_lemma)

            restriction_xpath = (
                '//*'
                '[@class="Obs__Texto"]'
                '[contains(text(), "só se emprega a forma")]'
            )
            restriction_text = selector.xpath(restriction_xpath).get()
            if restriction_text:
                match = re.search(
                    r'Como\s+(.+?)\s+só\s+se\s+emprega\s+a\s+forma\s+<i>(.*?)</i>',
                    restriction_text
                )
                if match and match[2] in feminine_lemmas:
                    pos_feminine_lemmas[match[1]].add(match[2])

            if not same_as:
                same_as = get_same_as(selector)

            better_forms_xpath = (
                '//*[@class="References"]'
                '[re:test(text(), "Formas?\s+máis\s+recomendables?:")]'
                '//@data-noun')
            better_forms = set(selector.xpath(better_forms_xpath).getall())

            pos_css = ('.Lemma > .Subentry > .Subentry__Part_of_speech::text, '
                       '.Sense__PartOfSpeech::text')
            transposcat_css = '.Transcategorizacion__Categoria::text'
            parts_of_speech = set(
                part_of_speech
                for part_of_speech in chain(
                    selector.css(pos_css).getall(),
                    selector.css(transposcat_css).getall(),
                )
                if part_of_speech.strip()
            )

            if not parts_of_speech:
                # Se non se indican as categorías gramaticais, recorrer ás
                # primeiras palabras da definición.
                xpath = '//*[@class="Definition__Definition"]/text()[1]'
                definitions = selector.xpath(xpath).getall()
                parts_of_speech = set()
                pof_map = {
                    'abreviatura': 'abreviatura',
                    'forma abreviada': 'abreviatura',
                    'sigla': 'sigla',
                    'símbolo': 'símbolo',
                }
                for definition in definitions:
                    definition = definition.strip().lower()
                    for prefix, part_of_speech in pof_map.items():
                        if definition.startswith(prefix):
                            parts_of_speech.add(part_of_speech)
                            break
                    else:
                        logger.error(
                            f'Could not determine the part of speech matching '
                            f'definition {definition!r} of {word!r} based on '
                            f'known starting words'
                        )

            nested_parts_of_speech = defaultdict(set)
            # «Tamén substantivo» en https://academia.gal/dicionario/-/termo/alterador
            transpos_css = '.Example__Transcategorizacion'
            for pos_selector in selector.css(transpos_css):
                transpos_parent_xpath = (
                    './/ancestor::*[@class="Subentry"]'
                    '//*[@class="Subentry__Part_of_speech"]/text()'
                )
                parent = pos_selector.xpath(transpos_parent_xpath).get()
                if parent is None:
                    logger.error(
                        f'No parent found for nested part of speech {pos} '
                        f'of {word} in: {selector.get()}')
                for pos in pos_selector.xpath('./text()').re(r'Tamén\s+(.*)'):
                    parts_of_speech.add(pos)
                    if parent is not None:
                        nested_parts_of_speech[parent].add(pos)

            invariable = False
            invariable_xpath = ('//*[@class="Obs__Texto"]'
                                '/text()[contains(., "invariable")]')
            invariable_text = selector.xpath(invariable_xpath).get()
            if invariable_text:
                match = re.search(
                    r'Como\s+(.+?)\s+é\s+invariable\s+en\s+canto\s+ao\s+(número|xénero)',
                    invariable_text
                )
                if match:
                    invariable = {match[1]: match[2]}
                else:
                    match = re.search(
                        r'Como\s+(.+?)\s+é\s+invariable',
                        invariable_text
                    )
                    if match:
                        invariable = {match[1]: True}
                    else:
                        invariable = True

            key = (
                frozenset(better_forms),
                (
                    invariable if isinstance(invariable, bool)
                    else tuple(sorted(invariable.items()))
                ),
            )
            if key in subentries:
                subentry = subentries[key]
                for parent, poses in nested_parts_of_speech.items():
                    subentry['nested_parts_of_speech'][parent] |= poses
                subentry['parts_of_speech'] |= parts_of_speech
            else:
                subentries[key] = {
                    'better_forms': better_forms,
                    'invariable': invariable,
                    'nested_parts_of_speech': nested_parts_of_speech,
                    'parts_of_speech': parts_of_speech,
                }
        for subentry in subentries.values():
            yield {
                **subentry,
                'lemma': word,
                'feminine_lemmas': sorted(feminine_lemmas),
                'pos_feminine_lemmas': {k: sorted(v) for k, v
                                        in pos_feminine_lemmas.items()},
                'same_as': same_as,
            }

        crawl_directions = ('previous', 'next')
        if browse in crawl_directions:
            yield self.entries_request(word, browse)
        elif browse == 'both':
            for direction in crawl_directions:
                yield self.entries_request(word, direction)

    def parse_entries(self, response):
        browse, entries = response.meta['browse'], json.loads(response.text)
        browse_index = 0 if browse == 'previous' else len(entries)-1
        for index, entry in enumerate(entries):
            meta = {}
            if index == browse_index:
                meta['browse'] = browse
            yield self.entry_request(entry['title'], **meta)
