from scrapy.crawler import CrawlerProcess

from drag.scraping.dragspider import DRAGSpider


SPIDER_CLASSES = (DRAGSpider,)


def scrape(jl_filepath, cache_dir):
    for spider_class in SPIDER_CLASSES:
        if not spider_class.is_online():
            continue
        process = CrawlerProcess({
            'FEEDS': {
                str(jl_filepath): {
                    'format': 'jsonlines',
                },
            },
            'HTTPCACHE_DIR': str(cache_dir / 'scrapy'),
        })
        process.crawl(spider_class)
        process.start()
        break
    else:
        raise RuntimeError(
            'None of the source websites is online at the moment'
        )
